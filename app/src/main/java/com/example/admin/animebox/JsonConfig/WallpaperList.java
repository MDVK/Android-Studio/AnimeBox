/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.JsonConfig;

import java.io.Serializable;

/**
 * Created by admin on 30-06-2017.
 */

public class WallpaperList implements Serializable {

    private String id,wallpaper_image,wallpaper_image_thumb,total_views,total_likes,total_download;

    public WallpaperList(String id, String wallpaper_image, String wallpaper_image_thumb, String total_views, String total_likes, String total_download) {
        this.id = id;
        this.wallpaper_image = wallpaper_image;
        this.wallpaper_image_thumb = wallpaper_image_thumb;
        this.total_views = total_views;
        this.total_likes = total_likes;
        this.total_download = total_download;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWallpaper_image() {
        return wallpaper_image;
    }

    public void setWallpaper_image(String wallpaper_image) {
        this.wallpaper_image = wallpaper_image;
    }

    public String getWallpaper_image_thumb() {
        return wallpaper_image_thumb;
    }

    public void setWallpaper_image_thumb(String wallpaper_image_thumb) {
        this.wallpaper_image_thumb = wallpaper_image_thumb;
    }

    public String getTotal_views() {
        return total_views;
    }

    public void setTotal_views(String total_views) {
        this.total_views = total_views;
    }

    public String getTotal_likes() {
        return total_likes;
    }

    public void setTotal_likes(String total_likes) {
        this.total_likes = total_likes;
    }

    public String getTotal_download() {
        return total_download;
    }

    public void setTotal_download(String total_download) {
        this.total_download = total_download;
    }
}
