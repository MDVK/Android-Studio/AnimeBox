/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.admin.animebox.APPMDVK.BaseActivity;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.wang.avi.AVLoadingIndicatorView;

public class SplashScreen extends BaseActivity {
    // splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private AVLoadingIndicatorView progressBar;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // FIX WEIRD BUG -__-
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_splash_screen);

        YoYo.with(Techniques.Tada)
                .duration(2000)
                .playOn(findViewById(R.id.tvTitle));

        YoYo.with(Techniques.BounceInUp)
                .duration(3000)
                .repeat(3)
                .playOn(findViewById(R.id.tvSubTitle));

        progressBar = (AVLoadingIndicatorView) findViewById(R.id.pb1);
        progressBar.isShown();
        progressBar.setIndicator("BallClipRotatePulseIndicator");

        // remove toolbar if > SDK 21
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        Constant_Api.forceRTLIfSupported(getWindow(), SplashScreen.this);

        if (Constant_Api.isNetworkAvailable(SplashScreen.this)) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent i = new Intent(SplashScreen.this, MainWallpaper.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        } else {

            SH_SB(getResources().getString(R.string.internet_connection));
            SH_ALERT_POLOS("Notify", getResources().getString(R.string.internet_connection_message));
        }
    }
}
