/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */



package com.example.admin.animebox.Activity;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.admin.animebox.APPMDVK.BaseActivity;
import com.example.admin.animebox.Adapter.WallpaperDetailAdapter;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class WallpaperDetail extends BaseActivity {

    public Bitmap mbitmap;
    public boolean croup = false;
    private WallpaperDetailAdapter wallpaperDetailAdapter;
    private int selectedPosition = 0;
    private ViewPager viewPager;
    //	page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            wallpaper_view_count();
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };
    private Toolbar toolbar;
    private ImageView imageView_download, imageView_setWallpaper, imageView_share;
    private Uri resultUri, uri;
    private int viewPager_Position = 0;
    private Constant_Api constant_api;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallpaper_detail);

        Constant_Api.forceRTLIfSupported(getWindow(),WallpaperDetail.this);

        constant_api = new Constant_Api(WallpaperDetail.this);

        Intent in = getIntent();
        selectedPosition = in.getIntExtra("position", 0);

        viewPager = (ViewPager) findViewById(R.id.viewPager_wallpaper_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar_wallpaper_detail);
        imageView_download = (ImageView) findViewById(R.id.imageView_download_wallpaper_detail);
        imageView_setWallpaper = (ImageView) findViewById(R.id.imageView_setWallpaper_wallpaper_detail);
        imageView_share = (ImageView) findViewById(R.id.imageView_share_wallpaper_detail);

        toolbar.setTitle(getResources().getString(R.string.app_name));
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.arrow_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        wallpaperDetailAdapter = new WallpaperDetailAdapter(Constant_Api.wallpaperLists, WallpaperDetail.this);
        wallpaperDetailAdapter.notifyDataSetChanged();
        viewPager.setAdapter(wallpaperDetailAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        viewPager.setOffscreenPageLimit(Constant_Api.wallpaperLists.size());
        setCurrentItem(selectedPosition);


        imageView_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant_Api.downlode = true;
                constant_api.ClassCall(Constant_Api.wallpaperLists.get(viewPager.getCurrentItem()).getWallpaper_image(),"wallpaper","");
                wallpaper_downlode(viewPager.getCurrentItem());
                Toast.makeText(WallpaperDetail.this, "download", Toast.LENGTH_SHORT).show();
            }
        });

        imageView_setWallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                croup = true;
                new DownloadImageTask().execute(Constant_Api.wallpaperLists.get(viewPager.getCurrentItem()).getWallpaper_image());
            }
        });

        imageView_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant_Api.share = true;
                constant_api.ClassCall(Constant_Api.wallpaperLists.get(viewPager.getCurrentItem()).getWallpaper_image(),"wallpaper","");
                Toast.makeText(WallpaperDetail.this, "Share", Toast.LENGTH_SHORT).show();
            }
        });

        if (viewPager.getCurrentItem() == 0) {
            wallpaper_view_count();
        }
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.wallpaper_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.ic_next:

                if (viewPager.getCurrentItem() == Constant_Api.wallpaperLists.size() - 1) {
                    Toast.makeText(WallpaperDetail.this, "You Have Alrady Last Position", Toast.LENGTH_SHORT).show();
                } else {
                    viewPager_Position = viewPager.getCurrentItem();
                    viewPager_Position++;
                    viewPager.setCurrentItem(viewPager_Position);
                }
                break;
            // action with ID action_settings was selected
            case R.id.ic_preview:
                if (viewPager.getCurrentItem() == 0) {
                    Toast.makeText(WallpaperDetail.this, "You Have Alrady First Position", Toast.LENGTH_SHORT).show();
                } else {
                    viewPager_Position = viewPager.getCurrentItem();
                    viewPager_Position--;
                    viewPager.setCurrentItem(viewPager_Position);
                }
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }

        return true;
    }

    private boolean croup_storeImage(Bitmap imageData) {
        //get path to external storage (SD card)
        String filePath;
        String iconsStoragePath = getExternalCacheDir().getAbsolutePath();
        try {
            String fname = "Image";
            filePath = iconsStoragePath + "/" + fname + ".jpg";
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            //choose another format if PNG doesn't suit you
            imageData.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();

        } catch (FileNotFoundException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        }

        uri = Uri.parse("file://" + filePath);
        CropImage.activity(uri).start(WallpaperDetail.this);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                try {
                    Bitmap myBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(resultUri));
                    set_Wallpaper(myBitmap);
                } catch (IOException e) {
                    System.out.println(e);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void set_Wallpaper(Bitmap bitmap) {

        WallpaperManager myWallpaperManager = WallpaperManager.getInstance(getApplicationContext());
        try {
            myWallpaperManager.setBitmap(bitmap);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void wallpaper_downlode(final int position) {

        final String url = Constant_Api.wallpaper_url_download + Constant_Api.wallpaperLists.get(position).getId();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                int value = Integer.parseInt(Constant_Api.wallpaperLists.get(position).getTotal_download());
                value++;
                Constant_Api.wallpaperLists.get(position).setTotal_download(String.valueOf(value));
                Log.d("wallpaper_downlode", String.valueOf(value));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void wallpaper_view_count() {

        final String url = Constant_Api.wallpaper_url_view + Constant_Api.wallpaperLists.get(viewPager.getCurrentItem()).getId();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                int value = Integer.parseInt(Constant_Api.wallpaperLists.get(viewPager.getCurrentItem()).getTotal_views());
                value++;
                Constant_Api.wallpaperLists.get(viewPager.getCurrentItem()).setTotal_views(String.valueOf(value));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public class DownloadImageTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                mbitmap = BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                // Log exception
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (croup == true) {
                croup_storeImage(mbitmap);
                croup = false;
            }
            super.onPostExecute(s);
        }
    }

}

