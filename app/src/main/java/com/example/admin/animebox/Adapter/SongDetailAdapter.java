/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.JsonConfig.SongList;
import com.example.admin.animebox.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 06-07-2017.
 */

public class SongDetailAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    private List<SongList> songLists = new ArrayList<>();
    private Activity activity;

    public SongDetailAdapter(List<SongList> songLists, Activity activity) {
        this.songLists = songLists;
        this.activity = activity;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.song_detail_adapter, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView_song_detail_adapter);
        TextView textView_singer = (TextView) view.findViewById(R.id.textView_singer_song_detail_adapter);
        TextView textView_music = (TextView) view.findViewById(R.id.textView_music_song_detail_adapter);
        TextView textView_lyrics = (TextView) view.findViewById(R.id.textView_lyrics_song_detail_adapter);
        TextView textView_musicLable = (TextView) view.findViewById(R.id.textView_musicLable_song_detail_adapter);
        TextView textView_songTitle = (TextView) view.findViewById(R.id.textview_song_detail_adapter);

        Glide.with(activity).load(songLists.get(position).getSong_thumbnail_b())
                .into(imageView);

        textView_singer.setText(songLists.get(position).getSinger());
        textView_music.setText(songLists.get(position).getMusic());
        textView_lyrics.setText(songLists.get(position).getLyrics());
        textView_musicLable.setText(songLists.get(position).getMusic_lable());
        textView_songTitle.setText(songLists.get(position).getSong_title());

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return songLists.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}

