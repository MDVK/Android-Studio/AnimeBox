/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.animebox.APPMDVK.BaseRandom;
import com.example.admin.animebox.Adapter.WallpaperAdapter;
import com.example.admin.animebox.JsonConfig.WallpaperList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class WallpaperFragment extends Fragment {

    private RecyclerView recyclerView;
    private AVLoadingIndicatorView progressBar;
    private WallpaperAdapter wallpaperAdapter;
    private List<WallpaperList> wallpaperLists;
    private AdView mAdView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.wallpaper_fragment, container, false);

        wallpaperLists = new ArrayList<>();
        progressBar = (AVLoadingIndicatorView) view.findViewById(R.id.progresbar_wallpaper_fragment);
        BaseRandom.setLoadingBar(progressBar);


        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_wallpaper_fragment);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        mAdView = (AdView) view.findViewById(R.id.adView_wallpaper_fragment);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (Constant_Api.isNetworkAvailable(getActivity())) {
            loadWallpaper();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            progressBar.hide();
        }

        return view;

    }

    public void loadWallpaper() {

        progressBar.isShown();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant_Api.wallpaper_url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                Log.d("Response", new String(responseBody));
                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.MDVK);
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String wallpaper_image = object.getString("wallpaper_image");
                        String wallpaper_image_thumb = object.getString("wallpaper_image_thumb");
                        String total_views = object.getString("total_views");
                        String total_likes = object.getString("total_likes");
                        String total_download = object.getString("total_download");

                        wallpaperLists.add(new WallpaperList(id, wallpaper_image, wallpaper_image_thumb, total_views, total_likes, total_download));

                    }

                    wallpaperAdapter = new WallpaperAdapter(getActivity(), wallpaperLists);
                    recyclerView.setAdapter(wallpaperAdapter);
                    progressBar.hide();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    @Override
    public void onResume() {
        if (wallpaperAdapter != null) {
            wallpaperAdapter.notifyDataSetChanged();
        }
        super.onResume();
    }
}
