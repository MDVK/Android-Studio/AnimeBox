/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.admin.animebox.APPMDVK.BaseRandom;
import com.example.admin.animebox.Activity.MainActivity;
import com.example.admin.animebox.Activity.MainWallpaper;
import com.example.admin.animebox.Adapter.HomeSongAdapter;
import com.example.admin.animebox.Adapter.HomeVideoAdapter;
import com.example.admin.animebox.Adapter.HomeWallpaperAdapter;
import com.example.admin.animebox.JsonConfig.SongList;
import com.example.admin.animebox.JsonConfig.VideoList;
import com.example.admin.animebox.JsonConfig.WallpaperList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class HomeFragment extends Fragment {

    InterstitialAd mInterstitialAd;
    AdRequest adRequest;
    private RecyclerView recyclerView_song, recyclerView_wallpaper, recyclerView_wallpaper2, recyclerView_video;
    private AVLoadingIndicatorView progressBar;
    private HomeSongAdapter homeSongAdapter;
    private HomeWallpaperAdapter homeWallpaperAdapter;
    private HomeVideoAdapter homeVideoAdapter;
    private Button button_songall, button_videoall, button_wallpaperall, button_wallpaperall2;
    private List<SongList> home_SongLists;
    private List<WallpaperList> home_WallpaperLists;
    private List<VideoList> home_VideoLists;
    private AdView mAdView;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.home_fragment, container, false);

        home_SongLists = new ArrayList<>();
        home_WallpaperLists = new ArrayList<>();
        home_VideoLists = new ArrayList<>();

        InterstitialAd();

        // Loading
        progressBar = (AVLoadingIndicatorView) view.findViewById(R.id.progresbar_home_fragment);
        BaseRandom.setLoadingBar(progressBar);

        button_songall = (Button) view.findViewById(R.id.button_songlist_viewall);
        button_videoall = (Button) view.findViewById(R.id.button_video_viewall);
        button_wallpaperall = (Button) view.findViewById(R.id.button_wallpaper_viewall);
        button_wallpaperall2 = (Button) view.findViewById(R.id.button_wallpaper_viewall_wibu);

        // Music
        recyclerView_song = (RecyclerView) view.findViewById(R.id.recyclerView_song_home_fragment);
        recyclerView_song.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager_song = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_song.setLayoutManager(layoutManager_song);

        // wallaper1
        recyclerView_wallpaper = (RecyclerView) view.findViewById(R.id.recyclerView_wallpaper_home_fragment);
        recyclerView_wallpaper.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager_wallpaper = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_wallpaper.setLayoutManager(layoutManager_wallpaper);

        // wallaper2
        recyclerView_wallpaper2 = (RecyclerView) view.findViewById(R.id.recyclerView_wallpaper_home_fragment_wibu);
        recyclerView_wallpaper2.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager_wallpaper2 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_wallpaper2.setLayoutManager(layoutManager_wallpaper2);

        // video
        recyclerView_video = (RecyclerView) view.findViewById(R.id.recyclerView_video_home_fragment);
        recyclerView_video.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager_video = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView_video.setLayoutManager(layoutManager_video);

        // ADS
        mAdView = (AdView) view.findViewById(R.id.adView_home_fragment);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (Constant_Api.isNetworkAvailable(getActivity())) {
            loadHome();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            progressBar.hide();
        }

        button_songall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            InterstitialAd();
                            songFragment();
                            super.onAdClosed();
                        }
                    });
                } else {
                    songFragment();
                }
            }
        });

        button_videoall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoFragment videoFragment = new VideoFragment();
                FragmentTransaction videoFragment_ft = getActivity().getSupportFragmentManager().beginTransaction();
                videoFragment_ft.replace(R.id.framlayout_main, videoFragment);
                videoFragment_ft.commit();
                (MainActivity.toolbar).setTitle(getResources().getString(R.string.video_list));
                Constant_Api.onBackPress = false;
            }
        });

        button_wallpaperall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                WallpaperFragment wallpaperFragment = new WallpaperFragment();
//                FragmentTransaction wallpaperFragment_ft = getActivity().getSupportFragmentManager().beginTransaction();
//                wallpaperFragment_ft.replace(R.id.framlayout_main, wallpaperFragment);
//                wallpaperFragment_ft.commit();
//                (MainActivity.toolbar).setTitle(getResources().getString(R.string.title_male_wallpaper));
//                Constant_Api.onBackPress = false;
                startActivity(new Intent(getActivity(), MainWallpaper.class));

            }
        });

        button_wallpaperall2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WallpaperFragment wallpaperFragment = new WallpaperFragment();
                FragmentTransaction wallpaperFragment_ft = getActivity().getSupportFragmentManager().beginTransaction();
                wallpaperFragment_ft.replace(R.id.framlayout_main, wallpaperFragment);
                wallpaperFragment_ft.commit();
                (MainActivity.toolbar).setTitle(getResources().getString(R.string.title_female_wallpaper));
                Constant_Api.onBackPress = false;
            }
        });

        return view;

    }

    public void songFragment() {
        SongFragment songFragment = new SongFragment();
        FragmentTransaction songFragment_ft = getActivity().getSupportFragmentManager().beginTransaction();
        songFragment_ft.replace(R.id.framlayout_main, songFragment);
        songFragment_ft.commitAllowingStateLoss();
        (MainActivity.toolbar).setTitle(getResources().getString(R.string.songs_list));
        Constant_Api.onBackPress = false;
    }

    public void InterstitialAd() {
        mInterstitialAd = new InterstitialAd(getActivity());
        mInterstitialAd.setAdUnitId(getActivity().getResources().getString(R.string.admob_interstitial_id));
        adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    public void loadHome() {

        progressBar.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant_Api.home_url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                Log.d("Response", new String(responseBody));
                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    // Head JSON Array
                    JSONObject object = jsonObject.getJSONObject(Constant_Api.MDVK);

                    /*
                    |----------------------------------------
                    |BEGIN HERE
                    |----------------------------------------*/

                    // Male Wallpaper
                    JSONArray jsonArray_wallpaper = object.getJSONArray("wallpaper_latest");

                    for (int i = 0; i < jsonArray_wallpaper.length(); i++) {

                        JSONObject object_wallpaper = jsonArray_wallpaper.getJSONObject(i);
                        String id = object_wallpaper.getString("id");
                        String wallpaper_image = object_wallpaper.getString("wallpaper_image");
                        String wallpaper_image_thumb = object_wallpaper.getString("wallpaper_image_thumb");
                        String total_views = object_wallpaper.getString("total_views");
                        String total_likes = object_wallpaper.getString("total_likes");
                        String total_download = object_wallpaper.getString("total_download");

                        home_WallpaperLists.add(new WallpaperList(id, wallpaper_image, wallpaper_image_thumb, total_views, total_likes, total_download));

                    }
                    homeWallpaperAdapter = new HomeWallpaperAdapter(getActivity(), home_WallpaperLists);
                    recyclerView_wallpaper.setAdapter(homeWallpaperAdapter);



                    // Anime AMV
                    JSONArray jsonArray_video = object.getJSONArray("video_latest");

                    for (int i = 0; i < jsonArray_video.length(); i++) {

                        JSONObject object_video = jsonArray_video.getJSONObject(i);
                        String id = object_video.getString("id");
                        String video_type = object_video.getString("video_type");
                        String video_title = object_video.getString("video_title");
                        String video_url = object_video.getString("video_url");
                        String video_id = object_video.getString("video_id");
                        String video_thumbnail_b = object_video.getString("video_thumbnail_b");
                        String video_thumbnail_s = object_video.getString("video_thumbnail_s");
                        String video_duration = object_video.getString("video_duration");
                        String video_description = object_video.getString("video_description");
                        String singer = object_video.getString("singer");
                        String music = object_video.getString("music");
                        String lyrics = object_video.getString("lyrics");
                        String music_lable = object_video.getString("music_lable");
                        String total_views = object_video.getString("total_views");

                        home_VideoLists.add(new VideoList(id, video_type, video_title, video_url, video_id, video_thumbnail_b, video_thumbnail_s, video_duration, video_description, singer, music, lyrics, music_lable, total_views));
                    }

                    homeVideoAdapter = new HomeVideoAdapter(getActivity(), home_VideoLists);
                    recyclerView_video.setAdapter(homeVideoAdapter);

                    JSONArray jsonArray_song = object.getJSONArray("latest_ringtone");

                    for (int i = 0; i < jsonArray_song.length(); i++) {

                        JSONObject object_song = jsonArray_song.getJSONObject(i);
                        String id = object_song.getString("id");
                        String song_type = object_song.getString("ringtone_type");
                        String song_title = object_song.getString("ringtone_title");
                        String duration = object_song.getString("duration");
                        String song_url = object_song.getString("ringtone_url");
                        String song_thumbnail_b = object_song.getString("ringtone_thumbnail_b");
                        String song_thumbnail_s = object_song.getString("ringtone_thumbnail_s");
                        String singer = object_song.getString("singer");
                        String music = object_song.getString("music");
                        String lyrics = object_song.getString("lyrics");
                        String music_lable = object_song.getString("music_lable");
                        String total_views = object_song.getString("total_views");
                        String total_download = object_song.getString("total_download");

                        home_SongLists.add(new SongList(id, song_type, song_title, duration, song_url, song_thumbnail_b, song_thumbnail_s, singer, music, lyrics, music_lable, total_views, total_download));

                    }
                    homeSongAdapter = new HomeSongAdapter(getActivity(), home_SongLists);
                    recyclerView_song.setAdapter(homeSongAdapter);
                    progressBar.hide();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.hide();
            }
        });

    }
}
