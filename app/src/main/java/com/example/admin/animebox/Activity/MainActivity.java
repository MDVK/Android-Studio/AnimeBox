/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.animebox.APPMDVK.BaseActivity;
import com.example.admin.animebox.Fragment.HomeFragment;
import com.example.admin.animebox.Fragment.MovieInfoFragment;
import com.example.admin.animebox.Fragment.SongFragment;
import com.example.admin.animebox.Fragment.VideoFragment;
import com.example.admin.animebox.Fragment.WallpaperFragment;
import com.example.admin.animebox.JsonConfig.AboutUsList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */
public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static Toolbar toolbar;
    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 101;
    boolean doubleBackToExitPressedOnce = false;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private TextView textView_appDevelopedBy;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Constant_Api.forceRTLIfSupported(getWindow(), MainActivity.this);

        textView_appDevelopedBy = (TextView) findViewById(R.id.textView_app_developed_by_activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.home));

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (Constant_Api.isNetworkAvailable(MainActivity.this)) {
            loadAppDetail();
        } else {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
        }

        HomeFragment homeFragment = new HomeFragment();
        FragmentTransaction homeFragment_ft = getSupportFragmentManager().beginTransaction();
        homeFragment_ft.replace(R.id.framlayout_main, homeFragment);
        homeFragment_ft.commit();
        Constant_Api.onBackPress = true;

        checkPer();

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                .init();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            if (Constant_Api.onBackPress) {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getResources().getString(R.string.Please_click_BACK_again_to_exit), Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                HomeFragment homeFragment = new HomeFragment();
                FragmentTransaction homeFragment_ft = getSupportFragmentManager().beginTransaction();
                homeFragment_ft.replace(R.id.framlayout_main, homeFragment);
                homeFragment_ft.commit();
                toolbar.setTitle(getResources().getString(R.string.home));
                Constant_Api.onBackPress = true;
            }


        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        //Checking if the item is in checked state or not, if not make it in checked state
        if (item.isChecked()) item.setChecked(false);
        else
            item.setChecked(true);

        //Closing drawer on item click
        drawer.closeDrawers();

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {

            case R.id.home:

                HomeFragment homeFragment = new HomeFragment();
                FragmentTransaction homeFragment_ft = getSupportFragmentManager().beginTransaction();
                homeFragment_ft.replace(R.id.framlayout_main, homeFragment);
                homeFragment_ft.commit();
                toolbar.setTitle(getResources().getString(R.string.home));
                Constant_Api.onBackPress = true;

                return true;

            case R.id.info:

                MovieInfoFragment infoFragment = new MovieInfoFragment();
                FragmentTransaction infoFragment_ft = getSupportFragmentManager().beginTransaction();
                infoFragment_ft.replace(R.id.framlayout_main, infoFragment);
                infoFragment_ft.commit();
                toolbar.setTitle(getResources().getString(R.string.movie_info));
                Constant_Api.onBackPress = false;
                return true;

            case R.id.songs:

                SongFragment songFragment = new SongFragment();
                FragmentTransaction songFragment_ft = getSupportFragmentManager().beginTransaction();
                songFragment_ft.replace(R.id.framlayout_main, songFragment);
                songFragment_ft.commit();
                toolbar.setTitle(getResources().getString(R.string.songs_list));
                Constant_Api.onBackPress = false;

                return true;


            case R.id.video:

                VideoFragment videoFragment = new VideoFragment();
                FragmentTransaction videoFragment_ft = getSupportFragmentManager().beginTransaction();
                videoFragment_ft.replace(R.id.framlayout_main, videoFragment);
                videoFragment_ft.commit();
                toolbar.setTitle(getResources().getString(R.string.video_list));
                Constant_Api.onBackPress = false;

                return true;

            case R.id.wallpapers:

                WallpaperFragment wallpaperFragment = new WallpaperFragment();
                FragmentTransaction wallpaperFragment_ft = getSupportFragmentManager().beginTransaction();
                wallpaperFragment_ft.replace(R.id.framlayout_main, wallpaperFragment);
                wallpaperFragment_ft.commit();
                toolbar.setTitle(getResources().getString(R.string.title_male_wallpaper));
                Constant_Api.onBackPress = false;

                return true;

            case R.id.rate_app:
                Uri uri = Uri.parse("market://details?id=" + getApplication().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getApplication().getPackageName())));
                }
                return true;

            case R.id.more_app:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.play_more_app))));
                return true;

            case R.id.share_app:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                    String sAux = "\n" +getResources().getString(R.string.Let_me_recommend_you_this_application) +"\n\n";
                    sAux = sAux + "https://play.google.com/store/apps/details?id=" + getApplication().getPackageName();
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
                return true;

            case R.id.about:
                startActivity(new Intent(MainActivity.this, AboutUs.class));
                return true;

            case R.id.privacy_policy:
                startActivity(new Intent(MainActivity.this, PrivacyPolice.class));
                return true;

            default:
                return true;
        }
    }

    public void checkPer() {
        if ((ContextCompat.checkSelfPermission(MainActivity.this, "android.permission.WRITE_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        boolean canUseExternalStorage = false;

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    canUseExternalStorage = true;
                }

                if (!canUseExternalStorage) {
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.cannot_use_save_permission), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void loadAppDetail() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant_Api.app_detail, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                Log.d("Response", new String(responseBody));
                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.MDVK);

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        String app_name = object.getString("app_name");
                        String app_logo = object.getString("app_logo");
                        String app_version = object.getString("app_version");
                        String app_author = object.getString("app_author");
                        String app_contact = object.getString("app_contact");
                        String app_email = object.getString("app_email");
                        String app_website = object.getString("app_website");
                        String app_description = object.getString("app_description");
                        String app_developed_by = object.getString("app_developed_by");
                        String app_privacy_policy = object.getString("app_privacy_policy");

                        Constant_Api.aboutUs = new AboutUsList(app_name, app_logo, app_version, app_author, app_contact, app_email, app_website, app_description, app_developed_by, app_privacy_policy);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                textView_appDevelopedBy.setText(Constant_Api.aboutUs.getApp_developed_by());

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    // This fires when a notification is opened by tapping on it or one is received while the app is running.
    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

        @Override
        public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {
            try {
                if (additionalData != null) {
                    if (additionalData.has("actionSelected"))
                        Log.d("OneSignalExample", "OneSignal notification button with id " + additionalData.getString("actionSelected") + " pressed");

                    Log.d("OneSignalExample", "Full additionalData:\n" + additionalData.toString());
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

}
