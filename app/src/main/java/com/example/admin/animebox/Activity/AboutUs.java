/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Activity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.APPMDVK.BaseActivity;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class AboutUs extends BaseActivity {

    public Toolbar toolbar;
    private TextView app_name, app_version, app_author, app_contact, app_email, app_website, app_description;
    private ImageView app_logo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        Constant_Api.forceRTLIfSupported(getWindow(), AboutUs.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar_about_us);
        toolbar.setTitle(getResources().getString(R.string.about_us));
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        // Costum Back Button
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.arrow_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        app_name = (TextView) findViewById(R.id.app_name_about_us);
        app_version = (TextView) findViewById(R.id.app_version_about_us);
        app_author = (TextView) findViewById(R.id.app_author_about_us);
        app_contact = (TextView) findViewById(R.id.app_contact_about_us);
        app_email = (TextView) findViewById(R.id.app_email_about_us);
        app_website = (TextView) findViewById(R.id.app_website_about_us);
        app_description = (TextView) findViewById(R.id.app_description_about_us);

        app_logo = (ImageView) findViewById(R.id.app_logo_about_us);

        app_name.setText(Constant_Api.aboutUs.getApp_name());

        // Load App Information from Server with API
        Glide.with(getApplication()).load(Constant_Api.aboutUs.getApp_logo())
                .into(app_logo);

        app_version.setText(Constant_Api.aboutUs.getApp_version());
        app_author.setText(Constant_Api.aboutUs.getApp_author());
        app_contact.setText(Constant_Api.aboutUs.getApp_contact());
        app_email.setText(Constant_Api.aboutUs.getApp_email());
        app_website.setText(Constant_Api.aboutUs.getApp_website());
        app_description.setText(Html.fromHtml(Constant_Api.aboutUs.getApp_description()));

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
