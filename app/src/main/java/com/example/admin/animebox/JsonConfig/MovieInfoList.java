/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.JsonConfig;

import java.io.Serializable;

/**
 * Created by admin on 21-07-2017.
 */

public class MovieInfoList implements Serializable {

    private String title,sub_title,video_url,video_id,video_image,description,date;

    public MovieInfoList(String title, String sub_title, String video_url, String video_id, String video_image, String description, String date) {
        this.title = title;
        this.sub_title = sub_title;
        this.video_url = video_url;
        this.video_id = video_id;
        this.video_image = video_image;
        this.description = description;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_image() {
        return video_image;
    }

    public void setVideo_image(String video_image) {
        this.video_image = video_image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
