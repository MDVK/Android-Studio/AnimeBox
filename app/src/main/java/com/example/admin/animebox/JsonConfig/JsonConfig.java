/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.JsonConfig;

import java.io.Serializable;

public class JsonConfig implements Serializable {

    public static final String LATEST_ARRAY_NAME = "MaterialWallpaper";
    public static final String LATEST_IMAGE_CATEGORY_NAME = "category_name";
    public static final String LATEST_IMAGE_URL = "image";
    public static final String CATEGORY_ARRAY_NAME = "MaterialWallpaper";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_CID = "cid";
    public static final String CATEGORY_IMAGE_URL = "category_image";
    public static final String CATEGORY_ITEM_ARRAY = "MaterialWallpaper";
    public static final String CATEGORY_ITEM_CATNAME = "cat_name";
    public static final String CATEGORY_ITEM_IMAGEURL = "images";
    public static final String CATEGORY_ITEM_CATID = "cid";
    private static final long serialVersionUID = 1L;
    public static String CATEGORY_ITEM_CATIDD;
    public static String CATEGORY_TITLE;
    public static String CATEGORY_ID;

}
