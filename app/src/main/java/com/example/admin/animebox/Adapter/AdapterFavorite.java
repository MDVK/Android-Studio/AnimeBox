/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.ConfigWallpaper;
import com.example.admin.animebox.utilities.Pojo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterFavorite extends BaseAdapter {

    LayoutInflater layoutInflater;
    Activity activity;
    Pojo object;
    private List<Pojo> data;
    private int imageWidth;

    public AdapterFavorite(List<Pojo> contactList, Activity activity, int columnWidth) {
        this.activity = activity;
        this.data = contactList;
        layoutInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        this.imageWidth = columnWidth;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        object = data.get(position);

        View v = null;
        final GroupItem item = new GroupItem();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            v = layoutInflater.inflate(R.layout.lsv_item_grid_wallpaper, null);
        } else {
            v = layoutInflater.inflate(R.layout.lsv_item_grid_wallpaper_pre, null);
        }

        item.img_fav = (ImageView) v.findViewById(R.id.item);

        Picasso
                .with(activity)
                .load(ConfigWallpaper.SERVER_URL + "/upload/thumbs/" + object.getImageurl())
                .placeholder(R.drawable.ic_thumbnail)
                .into(item.img_fav);

        return v;
    }

    class GroupItem {
        public ImageView img_fav;
    }


}
