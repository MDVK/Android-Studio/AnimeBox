/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */


package com.example.admin.animebox.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.animebox.APPMDVK.BaseActivity;
import com.example.admin.animebox.Adapter.SongDetailAdapter;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.example.admin.animebox.Util.Method;
import com.example.admin.animebox.Util.Utilities;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;

public class SongDetail extends BaseActivity implements SeekBar.OnSeekBarChangeListener {

    private SongDetailAdapter songDetailAdapter;
    private int selectedPosition = 0, viewPagerPosition = 0;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private TextView textView_songTitle, textView_songStartTime, textView_songEndtime;
    private SimpleExoPlayer simpleExoPlayer;
    private SeekBar songProgressBar;
    private ImageView imageView_play, imageView_next, imageView_previous, imageView_volume, imageView_downlode, imageView_shuffle, imageView_repeat;
    private boolean isplay = true, isFirst = true, isShuffle = false, isRepeat = false;
    private ProgressDialog progressDialog;
    private Animation myAnim;
    private Handler mHandler = new Handler();
    private Utilities utils;
    private Constant_Api constant_api;
    private Method method;
    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            String totalDuration = Constant_Api.songLists.get(viewPagerPosition).getDuration();
            long currentDuration = simpleExoPlayer.getCurrentPosition();
            // Displaying Total Duration time
            textView_songEndtime.setText(totalDuration);
            // Displaying time completed playing
            textView_songStartTime.setText(String.format("%d:%d",
                    TimeUnit.MILLISECONDS.toMinutes(currentDuration),
                    TimeUnit.MILLISECONDS.toSeconds(currentDuration) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(currentDuration))
            ));
            // Updating progress bar
            int progress = utils.getProgressPercentage(currentDuration, Utilities.convert_long(totalDuration));
            songProgressBar.setProgress(progress);
            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };
    //	page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            viewPagerPosition = viewPager.getCurrentItem();
            changeViewData(viewPagerPosition);
            if (!isFirst) {
                playSong();
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_detail);

        Constant_Api.forceRTLIfSupported(getWindow(),SongDetail.this);

        utils = new Utilities();
        constant_api = new Constant_Api(SongDetail.this);
        method = new Method(SongDetail.this);
        progressDialog=new ProgressDialog(SongDetail.this);

        Intent in = getIntent();
        selectedPosition = in.getIntExtra("position", 0);

        viewPager = (ViewPager) findViewById(R.id.viewPager_song_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar_song_detail);
        textView_songTitle = (TextView) findViewById(R.id.textView_song_title_song_detail);
        songProgressBar = (SeekBar) findViewById(R.id.seekBar_song_detail);
        imageView_play = (ImageView) findViewById(R.id.imageView_play_song_detail);
        imageView_previous = (ImageView) findViewById(R.id.imageView_previous_song_detail);
        imageView_next = (ImageView) findViewById(R.id.imageView_next_song_detail);
        imageView_volume = (ImageView) findViewById(R.id.imageView_volume_song_detail);
        imageView_downlode = (ImageView) findViewById(R.id.imageView_downlode_song_detail);
        imageView_repeat = (ImageView) findViewById(R.id.imageView_repeat_song_detail);
        imageView_shuffle = (ImageView) findViewById(R.id.imageView_shuffle_song_detail);
        textView_songStartTime = (TextView) findViewById(R.id.textView_songStartTime_song_detail);
        textView_songEndtime = (TextView) findViewById(R.id.textView_songEndtTime_song_detail);

        toolbar.setTitle("Anime Music");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.arrow_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);

        songDetailAdapter = new SongDetailAdapter(Constant_Api.songLists, SongDetail.this);
        viewPager.setAdapter(songDetailAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        viewPager.setOffscreenPageLimit(Constant_Api.songLists.size());
        setCurrentItem(selectedPosition);

        textView_songTitle.setText(Constant_Api.songLists.get(viewPager.getCurrentItem()).getSong_title());
        textView_songTitle.setSingleLine(true);
        textView_songTitle.setSelected(true);
        songProgressBar.setOnSeekBarChangeListener(SongDetail.this);

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(getApplication(), trackSelector);
        simpleExoPlayer.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playbackState == SimpleExoPlayer.STATE_ENDED) {
                    onCompletion();
                }

                if (playbackState == SimpleExoPlayer.STATE_READY && playWhenReady) {
                    //simpleExoPlayer.setPlayWhenReady(true);
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }
        });

        imageView_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isplay) {
                    if (simpleExoPlayer != null) {
                        simpleExoPlayer.setPlayWhenReady(false);
                        // Changing download_button image to play download_button
                        imageView_play.setImageResource(R.drawable.play_song_button);
                        isplay = false;

                    }
                } else {
                    // Resume song
                    if (simpleExoPlayer != null) {
                        // Changing download_button image to pause download_button
                        simpleExoPlayer.setPlayWhenReady(true);
                        imageView_play.setImageResource(R.drawable.pause_song_button);
                        isplay = true;
                    }
                }
            }
        });

        imageView_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPagerPosition < (Constant_Api.songLists.size() - 1)) {
                    viewPagerPosition++;
                    playSong();
                } else {
                    viewPagerPosition = 0;
                    playSong();
                }
                changeViewData(viewPagerPosition);
               /* imageView_next.setImageResource(R.drawable.ic_next_musichove);
                imageView_previous.setImageResource(R.drawable.ic_prev);*/
            }
        });

        imageView_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPagerPosition > 0) {
                    viewPagerPosition--;
                    playSong();
                } else {
                    // play last song
                    viewPagerPosition = Constant_Api.songLists.size() - 1;
                    playSong();
                }
                changeViewData(viewPagerPosition);
               /* imageView_next.setImageResource(R.drawable.ic_next_music);
                imageView_previous.setImageResource(R.drawable.ic_prevhove);*/
            }
        });

        imageView_volume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView_volume.startAnimation(myAnim);
                final Dialog dialog = new Dialog(SongDetail.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog);
                dialog.getWindow().setLayout(ViewPager.LayoutParams.FILL_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
                SeekBar seekBarVolume = (SeekBar) dialog.findViewById(R.id.seekBarVolume_song_detail);
                final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                seekBarVolume.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
                seekBarVolume.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                seekBarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                });
                dialog.show();
            }
        });

        imageView_downlode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView_downlode.startAnimation(myAnim);
                song_downlode();
                method.download(viewPagerPosition, SongDetail.this);
                Toast.makeText(SongDetail.this, "download", Toast.LENGTH_SHORT).show();
            }
        });

        imageView_repeat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (isRepeat) {
                    isRepeat = false;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.repeat_is_off), Toast.LENGTH_SHORT).show();
                    imageView_repeat.setImageResource(R.drawable.ic_repeat);
                } else {
                    // make repeat to true
                    isRepeat = true;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.repeat_is_on), Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    isShuffle = false;
                    imageView_repeat.setImageResource(R.drawable.ic_repeathov);
                    imageView_shuffle.setImageResource(R.drawable.ic_shuffle);
                }
            }
        });

        imageView_shuffle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (isShuffle) {
                    isShuffle = false;
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.shuffle_is_off), Toast.LENGTH_SHORT).show();
                    imageView_shuffle.setImageResource(R.drawable.ic_shuffle);
                } else {
                    // make repeat to true
                    isShuffle = true;
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.shuffle_is_on), Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    isRepeat = false;
                    imageView_shuffle.setImageResource(R.drawable.ic_shufflehov);
                    imageView_repeat.setImageResource(R.drawable.ic_repeat);
                }
            }
        });

        playSong();
        isFirst = false;

    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.song_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.ic_share_song:
                Constant_Api.share = true;
                constant_api.ClassCall(Constant_Api.songLists.get(viewPager.getCurrentItem()).getSong_thumbnail_b(),"song","");
                Toast.makeText(SongDetail.this, "Share", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ic_down_top:
                song_downlode();
                method.download(viewPagerPosition, SongDetail.this);
                Toast.makeText(SongDetail.this, "download", Toast.LENGTH_SHORT).show();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = (int) simpleExoPlayer.getDuration();
        int currentPosition = Utilities.progressToTimer(seekBar.getProgress(), totalDuration);
        // forward or backward to certain seconds
        simpleExoPlayer.seekTo(currentPosition);
        // update timer progress again
        updateProgressBar();
    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 1000);
    }

    public void playSong() {
        imageView_play.setImageResource(R.drawable.pause_song_button);
        new Loading().execute();
        songDetailAdapter.notifyDataSetChanged();
        songProgressBar.setProgress(0);
        songProgressBar.setSecondaryProgress(0);
        songProgressBar.setMax(100);
        // Updating progress bar
        updateProgressBar();
    }

    /**
     * On Song Playing completed * if repeat is ON play same song again * if shuffle is ON play setLoadingBar song
     */

    public void onCompletion() {
        // check for repeat is ON or OFF
        if (isRepeat) {
            // repeat is on play same song again
            playSong();
        } else if (isShuffle) {
            // shuffle is on - play a setLoadingBar song
            Random rand = new Random();
            viewPagerPosition = rand.nextInt((Constant_Api.songLists.size() - 1) - 0 + 1) + 0;
            playSong();
        } else {
            // no repeat or shuffle ON - play next song
            if (viewPagerPosition < (Constant_Api.songLists.size() - 1)) {
                viewPagerPosition = viewPagerPosition + 1;
                playSong();
            } else {
                // play first song
                viewPagerPosition = 0;
                playSong();
            }
        }
        changeViewData(viewPagerPosition);
    }

    public void changeViewData(int selectedPosition) {
        textView_songTitle.setText(Constant_Api.songLists.get(selectedPosition).getSong_title());
        textView_songTitle.setSingleLine(true);
        textView_songTitle.setSelected(true);
        viewPager.setCurrentItem(selectedPosition);
    }

    public void song_downlode() {
        final String url = Constant_Api.song_downlode + Constant_Api.songLists.get(viewPager.getCurrentItem()).getId();
        Log.d("url", url);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                int value = Integer.parseInt(Constant_Api.songLists.get(viewPager.getCurrentItem()).getTotal_download());
                value++;
                Constant_Api.songLists.get(viewPager.getCurrentItem()).setTotal_download(String.valueOf(value));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        simpleExoPlayer.setPlayWhenReady(false);
        simpleExoPlayer.stop();
        super.onBackPressed();
    }

    class Loading extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            progressDialog.setMessage(getResources().getString(R.string.please_wait_loading));
            progressDialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            String url = Constant_Api.songLists.get(viewPagerPosition).getSong_url();
            // Measures bandwidth during playback. Can be null if not required.
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            // Produces DataSource instances through which media data is loaded.
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplication(), Util.getUserAgent(getApplication(), "yourApplicationName"), bandwidthMeter);
            // Produces Extractor instances for parsing the media data.
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            // This is the MediaSource representing the media to be played.
            MediaSource videoSource = new ExtractorMediaSource(Uri.parse(url), dataSourceFactory, extractorsFactory, null, null);
            // Prepare the player with the source.
            simpleExoPlayer.prepare(videoSource);
            simpleExoPlayer.setPlayWhenReady(true);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}


