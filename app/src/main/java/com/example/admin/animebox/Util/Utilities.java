/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by viaviweb-2 on 27-Jun-17.
 */

public class Utilities {

    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */

   /* public String a(String s) throws Exception {

        Pattern p = Pattern.compile("(\\d+):(\\d+)");
        Matcher m = p.matcher(s);
        if (m.matches()) {
            int hrs = Integer.parseInt(m.group(1));
            int min = Integer.parseInt(m.group(2));
            long ms = (long) hrs * 60 * 60 * 1000 + min * 60 * 1000;
            System.out.println("hrs=" + hrs + " min=" + min + " ms=" + ms);
        } else {
            throw new Exception("Bad time format");
        }
        return s;
    }*/
    public static long convert_long(String s) {

        long ms = 0;
        Pattern p;
        if (s.contains(("\\:"))) {
            p = Pattern.compile("(\\d+):(\\d+)");
        } else {
            p = Pattern.compile("(\\d+).(\\d+)");
        }
        Matcher m = p.matcher(s);
        if (m.matches()) {
            int min = Integer.parseInt(m.group(1));
            int sec = Integer.parseInt(m.group(2));
            ms = (long) min * 60 * 1000 + sec * 1000;
            System.out.println(" min=" + min + "sec=" + sec + "ms=" + ms);
        }
        return ms;
    }


    /**
     * Function to get Progress percentage
     *
     * @param currentDuration
     * @param totalDuration
     */

    public int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);


        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage
        return percentage.intValue();
    }

    /**
     * Function to change progress to timer
     *
     * @param progress      -
     * @param totalDuration returns current duration in milliseconds
     */
    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }

}
