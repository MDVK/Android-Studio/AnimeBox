/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.models;

public class ItemWallpaperByCategory {


	private String ItemCategoryName;
	private String ItemImageUrl;
	private String ItemCatId;
 

	public ItemWallpaperByCategory(String itemcategoryname, String itemimageurl, String itemcatid) {
		// TODO Auto-generated constructor stub
		this.ItemCategoryName=itemcategoryname;
		this.ItemImageUrl=itemimageurl;
		this.ItemCatId=itemcatid;
	}

	public ItemWallpaperByCategory() {
		// TODO Auto-generated constructor stub
	}

	public String getItemCategoryName() {
		return ItemCategoryName;
	}

	public void setItemCategoryName(String itemcategoryname) {
		this.ItemCategoryName = itemcategoryname;
	}


	public String getItemImageurl()
	{
		return ItemImageUrl;

	}

	public void setItemImageurl(String itemimageurl)
	{
		this.ItemImageUrl=itemimageurl;
	}
	public String getItemCatId()
	{
		return ItemCatId;

	}

	public void setItemCatId(String itemcatid)
	{
		this.ItemCatId=itemcatid;
	}


}
