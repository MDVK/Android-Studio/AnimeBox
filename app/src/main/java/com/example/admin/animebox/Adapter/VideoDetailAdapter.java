/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.admin.animebox.Activity.VideoPlayer;
import com.example.admin.animebox.JsonConfig.VideoList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.example.admin.animebox.Vimeo.Vimeo;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import java.util.List;

/**
 * Created by admin on 05-07-2017.
 */

public class VideoDetailAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    private List<VideoList> videoLists;
    private Activity activity;

    public VideoDetailAdapter(List<VideoList> videoLists, Activity activity) {
        this.videoLists = videoLists;
        this.activity = activity;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(R.layout.video_detail_adapter, container, false);

        final ImageView imageView = (ImageView) view.findViewById(R.id.imageView_video_detail_adapter);
        final ImageView imageView_play = (ImageView) view.findViewById(R.id.imageView_play_video_detail_adapter);
        final TextView textView_detail = (TextView) view.findViewById(R.id.textView_detail_video_detai_adapter);
        TextView textView_singer = (TextView) view.findViewById(R.id.textView_singer_video_detail_adapter);
        TextView textView_music = (TextView) view.findViewById(R.id.textView_music_video_detail_adapter);
        TextView textView_lyrics = (TextView) view.findViewById(R.id.textView_lyrics_video_detail_adapter);
        TextView textView_musicLable = (TextView) view.findViewById(R.id.textView_musicLable_video_detail_adapter);
        final TextView textView_title = (TextView) view.findViewById(R.id.textView_title_video_detail_adapter);
        final TextView textView_duration = (TextView) view.findViewById(R.id.texview_duration_video_detail_adapter);

        final ProgressBar progressBar_dailymotion = (ProgressBar) view.findViewById(R.id.progresbar_dailymotion);

        Glide.with(activity).load(videoLists.get(position).getVideo_thumbnail_b())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar_dailymotion.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar_dailymotion.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageView);

        textView_detail.setText(Html.fromHtml(videoLists.get(position).getVideo_description()));
        textView_singer.setText(videoLists.get(position).getSinger());
        textView_music.setText(videoLists.get(position).getMusic());
        textView_lyrics.setText(videoLists.get(position).getLyrics());
        textView_musicLable.setText(videoLists.get(position).getMusic_lable());
        textView_title.setText(videoLists.get(position).getVideo_title());
        textView_duration.setText(videoLists.get(position).getVideo_duration());

        imageView_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (videoLists.get(position).getVideo_type().equals("youtube")) {

                    if (Constant_Api.isAppInstalled(activity)) {
                        Intent intent = YouTubeStandalonePlayer.createVideoIntent(activity, Constant_Api.YOUR_DEVELOPER_KEY, videoLists.get(position).getVideo_id(), 0, true, false);
                        activity.startActivity(intent);
                    } else {
                        Toast.makeText(activity, R.string.youtube, Toast.LENGTH_SHORT).show();
                    }

                } else if (videoLists.get(position).getVideo_type().equals("dailymotion")) {

                    Intent vp_intent = new Intent(activity, VideoPlayer.class);
                    vp_intent.putExtra("Video_type", videoLists.get(position).getVideo_type());
                    vp_intent.putExtra("Video_id", videoLists.get(position).getVideo_id());
                    activity.startActivity(vp_intent);

                } else if (videoLists.get(position).getVideo_type().equals("vimeo")) {

                    Intent i = new Intent(activity, Vimeo.class);
                    i.putExtra("id", videoLists.get(position).getVideo_id());
                    activity.startActivity(i);

                } else if (videoLists.get(position).getVideo_type().equals("local")) {

                    Intent vp_intent = new Intent(activity, VideoPlayer.class);
                    vp_intent.putExtra("Video_url", videoLists.get(position).getVideo_url());
                    vp_intent.putExtra("Video_type", videoLists.get(position).getVideo_type());
                    activity.startActivity(vp_intent);

                } else if (videoLists.get(position).getVideo_type().equals("server_url")) {

                    Intent vp_intent = new Intent(activity, VideoPlayer.class);
                    vp_intent.putExtra("Video_url", videoLists.get(position).getVideo_url());
                    vp_intent.putExtra("Video_type", videoLists.get(position).getVideo_type());
                    activity.startActivity(vp_intent);

                }

            }
        });

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return videoLists.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}