/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.Activity.VideoDetail;
import com.example.admin.animebox.JsonConfig.VideoList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

/* * -------------------------------------------- * Hello World, Welcome to my code . * this SingleMovie i made for purpose only * code create by admin on 29-06-2017. * -------------------------------------------- * have a question, contact me : * 0852 1708 7944 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    InterstitialAd mInterstitialAd;
    AdRequest adRequest;
    private Activity activity;
    private List<VideoList> videoLists;

    public VideoAdapter(Activity activity, List<VideoList> videoLists) {
        this.activity = activity;
        this.videoLists = videoLists;
        InterstitialAd();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.video_adapter, parent, false);
        return new VideoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Glide.with(activity).load(videoLists.get(position).getVideo_thumbnail_b()).into(holder.imageView_videoImage);
        holder.textView_videoTitle.setText(videoLists.get(position).getVideo_title());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            InterstitialAd();
                            Intent vd_intent = new Intent(activity, VideoDetail.class);
                            Constant_Api.videoLists.clear();
                            Constant_Api.videoLists.addAll(videoLists);
                            vd_intent.putExtra("position", position);
                            activity.startActivity(vd_intent);
                            super.onAdClosed();
                        }
                    });
                } else {
                    Intent vd_intent = new Intent(activity, VideoDetail.class);
                    Constant_Api.videoLists.clear();
                    Constant_Api.videoLists.addAll(videoLists);
                    vd_intent.putExtra("position", position);
                    activity.startActivity(vd_intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoLists.size();
    }

    public void setFilter(List<VideoList> videoModels) {
        videoLists = new ArrayList<>();
        videoLists.addAll(videoModels);
        notifyDataSetChanged();
    }

    private void InterstitialAd() {
        mInterstitialAd = new InterstitialAd(activity);
        mInterstitialAd.setAdUnitId(activity.getResources().getString(R.string.admob_interstitial_id));
        adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_videoTitle;
        private ImageView imageView_videoImage;
        private RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textView_videoTitle = (TextView) itemView.findViewById(R.id.textView_video_title_video_adapter);
            imageView_videoImage = (ImageView) itemView.findViewById(R.id.imageview_video_adapter);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_video_adapter);

        }
    }
}
