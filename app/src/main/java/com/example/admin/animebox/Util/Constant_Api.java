/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.admin.animebox.JsonConfig.AboutUsList;
import com.example.admin.animebox.JsonConfig.SongList;
import com.example.admin.animebox.JsonConfig.VideoList;
import com.example.admin.animebox.JsonConfig.WallpaperList;
import com.example.admin.animebox.R;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 30-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class Constant_Api {

    public static boolean share = false;
    public static boolean downlode = false;
    public static boolean onBackPress = false;
    public static Bitmap mbitmap;
    /*
    |----------------------------------------
    |       SETUP REST API :D
    |----------------------------------------
     */
    //main header jsonarray
    public static String MDVK ="MDVK_ANIME_BOX";
    //main URL
    public static String url = "http://apaini-as.cloud.revoluz.io/public/smp/";
    //youtube developer key
    public static String YOUR_DEVELOPER_KEY = "AIzaSyAidL0lPi-Kqqfp_kq9bq6q1rOK0IXa2RM";
    //app detail
    public static String app_detail = url + "api.php";
    //aerver api movie info url
    public static String movie_info = url + "api.php?movie_info";
    //server api home url
    public static String home_url = url + "api.php?home";
    //server api Wallpaper url
    public static String wallpaper_url = url + "api.php?wallpaper";
    //server api Wallpaper download url
    public static String wallpaper_url_view = wallpaper_url + "_id=";


    /* AREA WALLPAPER 1*/
    //server api Wallpaper download url
    public static String wallpaper_url_download = wallpaper_url + "_download=";
    //server api Wallpaper Wibu URL
    public static String wallpaper_url_2 = url + "api.php?wibu";
    //server api Wallpaper Wibu download url
    public static String wallpaper_url_view_2 = wallpaper_url_2 + "_id=";


    /* AREA WALLPAPER 2*/
    //server api Wallpaper download url
    public static String wallpaper_url_download_2 = wallpaper_url_2 + "_download=";
    //server api song url
    public static String song_url = url + "api.php?songs";
    //server api song url downlode
    public static String song_downlode = url + "api.php?song_download_id=";
    //server api video url
    public static String video_url = url + "api.php?videos";
    //wallpaper arrayforLoadingBar
    public static List<WallpaperList> wallpaperLists = new ArrayList<>();
    //songs arrayforLoadingBar
    public static List<SongList> songLists = new ArrayList<>();
    //video arrayforLoadingBar
    public static List<VideoList> videoLists = new ArrayList<>();
    //AboutUs data
    public static AboutUsList aboutUs;
    public Activity activity;

    public Constant_Api(Activity activity) {
        this.activity = activity;
    }

    public static void forceRTLIfSupported(Window window, Activity activity) {
        if (activity.getResources().getString(R.string.isRTL).equals("true")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                window.getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
        }
    }

    //network check
    public static boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //youtube application installation or not check
    public static boolean isAppInstalled(Activity activity) {
        String packageName = "com.google.android.youtube";
        Intent mIntent = activity.getPackageManager().getLaunchIntentForPackage(packageName);
        return mIntent != null;
    }

    public int getScreenWidth() {
        int columnWidth;
        WindowManager wm = (WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();

        point.x = display.getWidth();
        point.y = display.getHeight();

        columnWidth = point.x;
        return columnWidth;
    }

   /*<---------------------- class share & downlode and methodes ---------------------->*/

    public void ClassCall(String image, String type, String videoLink) {
        new DownloadImageTask().execute(image, type, videoLink);
    }

    private void Share_SaveImage(Bitmap finalBitmap, String type, String videoLink) {

        String Application_Link = "https://play.google.com/store/apps/details?id=" + activity.getApplication().getPackageName();

        String root = activity.getExternalCacheDir().getAbsolutePath();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image_share" + n + ".jpg";
        File file = new File(root, fname);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Uri contentUri = Uri.fromFile(file);
        Log.d("url", String.valueOf(contentUri));

        if (contentUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
            shareIntent.setData(contentUri);
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);

            if (!type.equals("wallpaper")) {
                if (type.equals("movieInfo")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, videoLink + "\n" + "\n" + Application_Link);
                } else {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, Application_Link);
                }
            }
            activity.startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        }

    }

    private boolean storeImage(Bitmap imageData, String filename) {
        //get path to external storage (SD card)
        String iconsStoragePath = Environment.getExternalStorageDirectory() + "/SingleMovie/";
        File sdIconStorageDir = new File(iconsStoragePath);
        //create storage directories, if they don't exist
        sdIconStorageDir.mkdirs();
        try {
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = "Image-" + n;
            String filePath = sdIconStorageDir.toString() + "/" + filename + fname + ".jpg";
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            //choose another format if PNG doesn't suit you
            imageData.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
        } catch (FileNotFoundException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        } catch (IOException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        }
        return true;
    }

    public class DownloadImageTask extends AsyncTask<String, String, String> {

        String type, videoLink;

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                type = params[1];
                videoLink = params[2];
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                mbitmap = BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                // Log exception
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if (share == true) {
                Share_SaveImage(mbitmap, type, videoLink);
                share = false;
            } else if (downlode == true) {
                storeImage(mbitmap, "");
                downlode = false;
            }
            super.onPostExecute(s);
        }
    }
     /*<---------------------- class share & downlode and methodes ---------------------->*/

}
