/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.animebox.APPMDVK.BaseRandom;
import com.example.admin.animebox.Adapter.SongAdapter;
import com.example.admin.animebox.JsonConfig.SongList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by admin on 29-06-2017.
 */

public class SongFragment extends Fragment {

    public MenuItem searchItem;
    private RecyclerView recyclerView;
    private AVLoadingIndicatorView progressBar;
    private SongAdapter songAdapter;
    private List<SongList> songLists;
    private AdView mAdView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.song_fragment, container, false);

        songLists = new ArrayList<>();

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_song_fragment);
        progressBar = (AVLoadingIndicatorView) view.findViewById(R.id.progresbar_song_fragment);
        BaseRandom.setLoadingBar(progressBar);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        mAdView = (AdView) view.findViewById(R.id.adView_song_fragment);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (Constant_Api.isNetworkAvailable(getActivity())) {
            loadSong();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            progressBar.hide();
        }

        setHasOptionsMenu(true);
        return view;
    }

    @Override

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.songfragment_menu, menu);

        searchItem = menu.findItem(R.id.ic_searchView_songFragment);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener((new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final List<SongList> filteredModelList = filter(songLists, newText);
                songAdapter.setFilter(filteredModelList);
                songAdapter.notifyDataSetChanged();

                return true;
            }
        }));

        MenuItemCompat.setOnActionExpandListener(searchItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        songAdapter.setFilter(songLists);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private List<SongList> filter(List<SongList> models, String query) {
        query = query.toLowerCase();
        final List<SongList> filteredModelList = new ArrayList<>();
        for (SongList model : models) {
            final String text = model.getSong_title().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void loadSong() {

        progressBar.isShown();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant_Api.song_url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                Log.d("Response", new String(responseBody));
                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.MDVK);

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String song_type = object.getString("ringtone_type");
                        String song_title = object.getString("ringtone_title");
                        String duration = object.getString("duration");
                        String song_url = object.getString("ringtone_url");
                        String song_thumbnail_b = object.getString("ringtone_thumbnail_b");
                        String song_thumbnail_s = object.getString("ringtone_thumbnail_s");
                        String singer = object.getString("singer");
                        String music = object.getString("music");
                        String lyrics = object.getString("lyrics");
                        String music_lable = object.getString("music_lable");
                        String total_views = object.getString("total_views");
                        String total_download = object.getString("total_download");

                        songLists.add(new SongList(id, song_type, song_title, duration, song_url, song_thumbnail_b, song_thumbnail_s, singer, music, lyrics, music_lable, total_views, total_download));

                    }

                    songAdapter = new SongAdapter(getActivity(), songLists);
                    recyclerView.setAdapter(songAdapter);
                    progressBar.hide();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    @Override
    public void onResume() {
        if (songAdapter != null) {
            songAdapter.notifyDataSetChanged();
        }
        super.onResume();
    }

}
