/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.JsonConfig;

import java.io.Serializable;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 03-07-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class VideoList implements Serializable {

    private String id,video_type,video_title,video_url,video_id,video_thumbnail_b,video_thumbnail_s,video_duration,video_description,singer,music,lyrics,music_lable,total_views;

    public VideoList(String id, String video_type, String video_title, String video_url, String video_id, String video_thumbnail_b, String video_thumbnail_s, String video_duration, String video_description, String singer, String music, String lyrics, String music_lable, String total_views) {
        this.id = id;
        this.video_type = video_type;
        this.video_title = video_title;
        this.video_url = video_url;
        this.video_id = video_id;
        this.video_thumbnail_b = video_thumbnail_b;
        this.video_thumbnail_s = video_thumbnail_s;
        this.video_duration = video_duration;
        this.video_description = video_description;
        this.singer = singer;
        this.music = music;
        this.lyrics = lyrics;
        this.music_lable = music_lable;
        this.total_views = total_views;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVideo_type() {
        return video_type;
    }

    public void setVideo_type(String video_type) {
        this.video_type = video_type;
    }

    public String getVideo_title() {
        return video_title;
    }

    public void setVideo_title(String video_title) {
        this.video_title = video_title;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_thumbnail_b() {
        return video_thumbnail_b;
    }

    public void setVideo_thumbnail_b(String video_thumbnail_b) {
        this.video_thumbnail_b = video_thumbnail_b;
    }

    public String getVideo_thumbnail_s() {
        return video_thumbnail_s;
    }

    public void setVideo_thumbnail_s(String video_thumbnail_s) {
        this.video_thumbnail_s = video_thumbnail_s;
    }

    public String getVideo_duration() {
        return video_duration;
    }

    public void setVideo_duration(String video_duration) {
        this.video_duration = video_duration;
    }

    public String getVideo_description() {
        return video_description;
    }

    public void setVideo_description(String video_description) {
        this.video_description = video_description;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public String getMusic_lable() {
        return music_lable;
    }

    public void setMusic_lable(String music_lable) {
        this.music_lable = music_lable;
    }

    public String getTotal_views() {
        return total_views;
    }

    public void setTotal_views(String total_views) {
        this.total_views = total_views;
    }
}
