/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.animebox.APPMDVK.BaseRandom;
import com.example.admin.animebox.Adapter.VideoAdapter;
import com.example.admin.animebox.JsonConfig.VideoList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by admin on 29-06-2017.
 */

public class VideoFragment extends Fragment{

    public MenuItem searchItem;
    private RecyclerView recyclerView;
    private AVLoadingIndicatorView progressBar;
    private VideoAdapter videoAdapter;
    private List<VideoList> videoLists;
    private AdView mAdView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.video_fragment, container, false);

        videoLists=new ArrayList<>();

        progressBar =(AVLoadingIndicatorView)view.findViewById(R.id.progresbar_video_fragment);
        BaseRandom.setLoadingBar(progressBar);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_video_fragment);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        mAdView = (AdView) view.findViewById(R.id.adView_video_fragment);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (Constant_Api.isNetworkAvailable(getActivity())){
            loadVideo();
        }else {
            Toast.makeText(getActivity(),getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            progressBar.hide();
        }

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.videofragment_menu, menu);

        searchItem = menu.findItem(R.id.ic_searchView);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener((new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final List<VideoList> filteredModelList = filter(videoLists, newText);
                videoAdapter.setFilter(filteredModelList);
                videoAdapter.notifyDataSetChanged();

                return true;
            }
        }));

        MenuItemCompat.setOnActionExpandListener(searchItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        videoAdapter.setFilter(videoLists);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private List<VideoList> filter(List<VideoList> models, String query) {
        query = query.toLowerCase();
        final List<VideoList> filteredModelList = new ArrayList<>();
        for (VideoList model : models) {
            final String text = model.getVideo_title().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void loadVideo() {

        progressBar.isShown();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant_Api.video_url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                Log.d("Response", new String(responseBody));
                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.MDVK);

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String video_type = object.getString("video_type");
                        String video_title = object.getString("video_title");
                        String video_url = object.getString("video_url");
                        String video_id = object.getString("video_id");
                        String video_thumbnail_b = object.getString("video_thumbnail_b");
                        String video_thumbnail_s = object.getString("video_thumbnail_s");
                        String video_duration = object.getString("video_duration");
                        String video_description = object.getString("video_description");
                        String singer = object.getString("singer");
                        String music = object.getString("music");
                        String lyrics = object.getString("lyrics");
                        String music_lable = object.getString("music_lable");
                        String total_views = object.getString("total_views");

                        videoLists.add(new VideoList(id, video_type, video_title, video_url, video_id, video_thumbnail_b, video_thumbnail_s, video_duration, video_description, singer, music, lyrics, music_lable, total_views));

                    }

                    videoAdapter = new VideoAdapter(getActivity(),videoLists);
                    recyclerView.setAdapter(videoAdapter);
                    progressBar.hide();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }
}
