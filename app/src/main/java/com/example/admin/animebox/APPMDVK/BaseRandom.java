/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.APPMDVK;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.Random;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by mlord on 11/26/2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class BaseRandom {
    public static final String [] arrayforLoadingBar = {
            "BallClipRotateIndicator",
            "BallClipRotatePulseIndicator",
            "SquareSpinIndicator",
            "BallClipRotateMultipleIndicator",
            "BallRotateIndicator",
            "CubeTransitionIndicator",
            "BallZigZagIndicator",
            "BallZigZagDeflectIndicator",
            "BallTrianglePathIndicator",
            "BallScaleIndicator",
            "LineScaleIndicator",
            "LineScalePartyIndicator",
            "BallScaleMultipleIndicator",
            "BallPulseSyncIndicator",
            "BallBeatIndicator",
            "LineScalePulseOutIndicator",
            "LineScalePulseOutRapidIndicator",
            "BallScaleRippleIndicator",
            "BallScaleRippleMultipleIndicator",
            "TriangleSkewSpinIndicator",
            "PacmanIndicator",
            "SemiCircleSpinIndicator"
    };
    public static String initStr = "";
    public AVLoadingIndicatorView progressBar;

    //constructor class
    public static void setLoadingBar(AVLoadingIndicatorView pbTarget){
        initStr = arrayforLoadingBar[new Random().nextInt(arrayforLoadingBar.length)];
        pbTarget.setIndicator(initStr);
    }
}
