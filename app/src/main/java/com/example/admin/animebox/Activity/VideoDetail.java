/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */


package com.example.admin.animebox.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.admin.animebox.APPMDVK.BaseActivity;
import com.example.admin.animebox.Adapter.VideoDetailAdapter;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;

public class VideoDetail extends BaseActivity {

    public Constant_Api constant_api;
    //	page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };
    private VideoDetailAdapter videoDetailAdapter;
    private int selectedPosition = 0;
    private ViewPager viewPager;
    private Toolbar toolbar;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        Constant_Api.forceRTLIfSupported(getWindow(),VideoDetail.this);

        constant_api = new Constant_Api(VideoDetail.this);

        Intent in = getIntent();
        selectedPosition = in.getIntExtra("position", 0);

        viewPager = (ViewPager) findViewById(R.id.viewPager_video_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar_video_detail);

        toolbar.setTitle(getResources().getString(R.string.app_name));
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.arrow_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        videoDetailAdapter = new VideoDetailAdapter(Constant_Api.videoLists, VideoDetail.this);
        viewPager.setAdapter(videoDetailAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        viewPager.setOffscreenPageLimit(Constant_Api.videoLists.size());
        setCurrentItem(selectedPosition);

    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.video_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.ic_share:
                Constant_Api.share = true;
                constant_api.ClassCall(Constant_Api.videoLists.get(viewPager.getCurrentItem()).getVideo_thumbnail_b(),"video","");
                Toast.makeText(VideoDetail.this, "Share", Toast.LENGTH_SHORT).show();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }

        return true;
    }

}
