/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.JsonConfig;

import java.io.Serializable;

/**
 * Created by admin on 30-06-2017.
 */

public class SongList implements Serializable {

    private String id,song_type,song_title,duration,song_url,song_thumbnail_b,song_thumbnail_s,singer,music,lyrics,music_lable,total_views,total_download;

    public SongList(String id, String song_type, String song_title, String duration, String song_url, String song_thumbnail_b, String song_thumbnail_s, String singer, String music, String lyrics, String music_lable, String total_views, String total_download) {
        this.id = id;
        this.song_type = song_type;
        this.song_title = song_title;
        this.duration = duration;
        this.song_url = song_url;
        this.song_thumbnail_b = song_thumbnail_b;
        this.song_thumbnail_s = song_thumbnail_s;
        this.singer = singer;
        this.music = music;
        this.lyrics = lyrics;
        this.music_lable = music_lable;
        this.total_views = total_views;
        this.total_download = total_download;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSong_type() {
        return song_type;
    }

    public void setSong_type(String song_type) {
        this.song_type = song_type;
    }

    public String getSong_title() {
        return song_title;
    }

    public void setSong_title(String song_title) {
        this.song_title = song_title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSong_url() {
        return song_url;
    }

    public void setSong_url(String song_url) {
        this.song_url = song_url;
    }

    public String getSong_thumbnail_b() {
        return song_thumbnail_b;
    }

    public void setSong_thumbnail_b(String song_thumbnail_b) {
        this.song_thumbnail_b = song_thumbnail_b;
    }

    public String getSong_thumbnail_s() {
        return song_thumbnail_s;
    }

    public void setSong_thumbnail_s(String song_thumbnail_s) {
        this.song_thumbnail_s = song_thumbnail_s;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public String getMusic_lable() {
        return music_lable;
    }

    public void setMusic_lable(String music_lable) {
        this.music_lable = music_lable;
    }

    public String getTotal_views() {
        return total_views;
    }

    public void setTotal_views(String total_views) {
        this.total_views = total_views;
    }

    public String getTotal_download() {
        return total_download;
    }

    public void setTotal_download(String total_download) {
        this.total_download = total_download;
    }
}
