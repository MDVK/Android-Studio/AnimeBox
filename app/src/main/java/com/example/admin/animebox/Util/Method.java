/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Util;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import com.example.admin.animebox.R;

import java.io.File;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 12-07-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class Method {

    public Activity activity;

    public Method(Activity activity) {
        this.activity = activity;
    }

    public void download(int viewPagerPosition, Activity activity) {
        File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SingleMovie/Music/");

        if (!root.exists()) {
            root.mkdirs();
        }

        File file = new File(root, Constant_Api.songLists.get(viewPagerPosition).getSong_title() + ".mp3");

        if (!file.exists()) {
            String url = Constant_Api.songLists.get(viewPagerPosition).getSong_url();
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDescription(activity.getResources().getString(R.string.downloading) + " - " + Constant_Api.songLists.get(viewPagerPosition).getSong_title());
            request.setTitle(Constant_Api.songLists.get(viewPagerPosition).getSong_title());
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir("/SingleMovie/Music", Constant_Api.songLists.get(viewPagerPosition).getSong_title() + ".mp3");

            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);

            //loadDownloaded(pos);
        } else {
            Toast.makeText(activity, activity.getResources().getString(R.string.already_downloaded), Toast.LENGTH_SHORT).show();
        }
    }

}
