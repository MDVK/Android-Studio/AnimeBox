/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.Activity.SongDetail;
import com.example.admin.animebox.JsonConfig.SongList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 30-06-2017.
 */

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder> {

    private Activity activity;
    private List<SongList> songLists;

    public SongAdapter(Activity activity, List<SongList> songLists) {
        this.activity = activity;
        this.songLists = songLists;
    }

    @Override
    public SongAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(activity).inflate(R.layout.song_adapter, parent, false);

        return new SongAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SongAdapter.ViewHolder holder, final int position) {

        Glide.with(activity).load(songLists.get(position).getSong_thumbnail_b()).into(holder.imageView_song);
        holder.textView_song_title.setText(songLists.get(position).getSong_title());
        holder.textView_downlode_count.setText(songLists.get(position).getTotal_download());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sd_intent = new Intent(activity, SongDetail.class);
                Constant_Api.songLists.clear();
                Constant_Api.songLists.addAll(songLists);
                sd_intent.putExtra("position", position);
                activity.startActivity(sd_intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return songLists.size();
    }

    public void setFilter(List<SongList> songListsModel) {
        songLists = new ArrayList<>();
        songLists.addAll(songListsModel);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView_song;
        private TextView textView_song_title, textView_downlode_count;
        private RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView_song = (ImageView) itemView.findViewById(R.id.imageView_song_adapter);
            textView_song_title = (TextView) itemView.findViewById(R.id.textview_songtitle_song_adapter);
            textView_downlode_count = (TextView) itemView.findViewById(R.id.textView_downlode_count_song_adapter);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_song_adapter);

        }
    }

}
