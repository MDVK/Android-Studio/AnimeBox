/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.Activity.VideoDetail;
import com.example.admin.animebox.JsonConfig.VideoList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;

import java.util.List;

/* * -------------------------------------------- * Hello World, Welcome to my code . * this SingleMovie i made for purpose only * code create by admin on 29-06-2017. * -------------------------------------------- * have a question, contact me : * 0852 1708 7944 */

public class HomeVideoAdapter extends RecyclerView.Adapter<HomeVideoAdapter.ViewHolder> {

    private Activity activity;
    private List<VideoList> home_videoLists;

    public HomeVideoAdapter(Activity activity, List<VideoList> home_videoLists) {
        this.activity = activity;
        this.home_videoLists = home_videoLists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.home_video_adapter, parent, false);

        return new HomeVideoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Glide.with(activity).load(home_videoLists.get(position).getVideo_thumbnail_b()).into(holder.imageView_video);
        holder.textView_videoTitle.setText(home_videoLists.get(position).getVideo_title());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vd_intent = new Intent(activity, VideoDetail.class);
                Constant_Api.videoLists.clear();
                Constant_Api.videoLists.addAll(home_videoLists);
                vd_intent.putExtra("position", position);
                activity.startActivity(vd_intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return home_videoLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_videoTitle;
        private ImageView imageView_video;
        private RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textView_videoTitle = (TextView) itemView.findViewById(R.id.textView_videoTitle_home_video_adapter);
            imageView_video = (ImageView) itemView.findViewById(R.id.imageView_home_video_adapter);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_home_video_adapter);
        }
    }
}
