package com.example.admin.animebox.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.animebox.APPMDVK.BaseActivity;
import com.example.admin.animebox.Fragment.FragmentCategory;
import com.example.admin.animebox.Fragment.FragmentFavorite;
import com.example.admin.animebox.Fragment.FragmentRecent;
import com.example.admin.animebox.R;

public class MainWallpaper extends BaseActivity {

    // Deklarasi

    TextView TitleToolBar;
    boolean klik2kali = false;
    private int[] IconTab = {
            R.drawable.ic_tab_recent,
            R.drawable.ic_tab_love,
            R.drawable.ic_tab_category
    };

//    -------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_wallpaper);


        // Inisialisasi
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TitleToolBar = (TextView) findViewById(R.id.toolbar_title);

        // Setup ToolBar
        setSupportActionBar(toolbar);
        setTitleToolBar("Recent Wallpaper ");
        toolbar.setTitleTextColor(getResources().getColor(R.color.itam));


        // Setup the viewPager
        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        if (viewPager != null)
            viewPager.setAdapter(pagerAdapter);

        // SetUp Tablayout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);

            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(pagerAdapter.getTabView(i));
            }

            tabLayout.getTabAt(0).getCustomView().setSelected(true);

            // Init viewPager -> change statement
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    switch (tab.getPosition()) {
                        case 0:
                            viewPager.setCurrentItem(0);
                            setTitleToolBar("Recent Wallpaper");
                            break;
                        case 1:
                            viewPager.setCurrentItem(1);
                            setTitleToolBar("My Favorite");
                            break;
                        case 2:
                            viewPager.setCurrentItem(2);
                            setTitleToolBar("Category !!");
                            break;

                        default:

                            viewPager.setCurrentItem(tab.getPosition());
                            setTitleToolBar("Recent Wallpaper");
                            break;
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }

    //    ToolbarTitle
    public void setTitleToolBar(String title) {
        TitleToolBar.setText(title);
    }


    // -- Function

    //    PagerAdapter
    private class MyPagerAdapter extends FragmentPagerAdapter {


        final int PAGE_COUNT = 3;
        private final String[] mTabsTitle = {"", "", ""};

        MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        View getTabView(int position) {
            View view = LayoutInflater.from(MainWallpaper.this).inflate(R.layout.custom_tab, null);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            icon.setImageResource(IconTab[position]);
            return view;
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return FragmentRecent.newInstance(1);
                case 1:
                    return FragmentFavorite.newInstance(2);
                case 2:
                    return FragmentCategory.newInstance(3);

            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabsTitle[position];
        }
    }
}