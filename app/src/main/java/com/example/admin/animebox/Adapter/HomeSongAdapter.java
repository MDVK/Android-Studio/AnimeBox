/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.Activity.SongDetail;
import com.example.admin.animebox.JsonConfig.SongList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;

import java.util.ArrayList;
import java.util.List;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 30-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class HomeSongAdapter extends RecyclerView.Adapter<HomeSongAdapter.ViewHolder> {

    private Activity activity;
    private List<SongList> home_songLists=new ArrayList<>();

    public HomeSongAdapter(Activity activity, List<SongList> home_songLists) {
        this.activity = activity;
        this.home_songLists = home_songLists;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(activity).inflate(R.layout.home_song_adapter,parent,false);

        return new HomeSongAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Glide.with(activity).load(home_songLists.get(position).getSong_thumbnail_b()).into(holder.imageView_song);
        holder.textView_songTitle.setText(home_songLists.get(position).getSong_title());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sd_intent=new Intent(activity, SongDetail.class);
                Constant_Api.songLists.clear();
                Constant_Api.songLists.addAll(home_songLists);
                sd_intent.putExtra("position",position);
                activity.startActivity(sd_intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return home_songLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textView_songTitle;
        private ImageView imageView_song;
        private RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textView_songTitle=(TextView)itemView.findViewById(R.id.textView_songTitle_home_song_adapter);
            imageView_song=(ImageView) itemView.findViewById(R.id.imageView_home_song_adapter);
            relativeLayout=(RelativeLayout) itemView.findViewById(R.id.relativeLayout_home_song_adapter);

        }
    }
}
