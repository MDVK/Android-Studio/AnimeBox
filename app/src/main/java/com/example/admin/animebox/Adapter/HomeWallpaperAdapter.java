/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.Activity.WallpaperDetail;
import com.example.admin.animebox.JsonConfig.WallpaperList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;

import java.util.List;
import java.util.Random;

/* * -------------------------------------------- * Hello World, Welcome to my code . * this SingleMovie i made for purpose only * code create by admin on 29-06-2017. * -------------------------------------------- * have a question, contact me : * 0852 1708 7944 */

public class HomeWallpaperAdapter  extends RecyclerView.Adapter<HomeWallpaperAdapter.ViewHolder> {

    private Activity activity;
    private List<WallpaperList> home_wallpaperLists;

    public HomeWallpaperAdapter(Activity activity, List<WallpaperList> home_wallpaperLists) {
        this.activity = activity;
        this.home_wallpaperLists = home_wallpaperLists;
    }

    @Override
    public HomeWallpaperAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(activity).inflate(R.layout.home_wallpaper_adapter,parent,false);

        return new  HomeWallpaperAdapter.ViewHolder(view);
    }

    public int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    @Override
    public void onBindViewHolder(HomeWallpaperAdapter.ViewHolder holder, final int position) {

        Glide.with(activity)
                .load(home_wallpaperLists.get(position).getWallpaper_image())
                .centerCrop()
                .fitCenter()
                .crossFade()
                .placeholder(getRandomColor())
                .into(holder.imageView_wallpaper);


        holder.imageView_wallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent wd_intent=new Intent(activity,WallpaperDetail.class);
                Constant_Api.wallpaperLists.clear();
                Constant_Api.wallpaperLists.addAll(home_wallpaperLists);
                wd_intent.putExtra("position",position);
                activity.startActivity(wd_intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return home_wallpaperLists.size();
    }

    public void testEr() {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView_wallpaper;

        public ViewHolder(View itemView) {
            super(itemView);
            //here you define your layout
            imageView_wallpaper=(ImageView)itemView.findViewById(R.id.imageView_home_wallpaper_adapter);
        }
    }
}

