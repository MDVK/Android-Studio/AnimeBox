/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */


package com.example.admin.animebox.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.dailymotion.android.player.sdk.PlayerWebView;
import com.example.admin.animebox.APPMDVK.BaseActivity;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;

import java.util.HashMap;

public class VideoPlayer extends BaseActivity implements EasyVideoCallback {

    private EasyVideoPlayer player;
    private PlayerWebView playerWebView;
    private String video_url, video_id, video_type;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        Constant_Api.forceRTLIfSupported(getWindow(),VideoPlayer.this);


        player = (EasyVideoPlayer) findViewById(R.id.player_video_deatil_adapter);
        playerWebView = (PlayerWebView) findViewById(R.id.dm_player_web_view);
        progressBar = (ProgressBar) findViewById(R.id.progresbar_video_play);

        Intent in = getIntent();
        video_url = in.getStringExtra("Video_url");
        video_type = in.getStringExtra("Video_type");
        video_id = in.getStringExtra("Video_id");


        if (video_type.equals("server_url")) {

            playerWebView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            player.setVisibility(View.VISIBLE);
            player.setCallback(this);
            player.setSource(Uri.parse(video_url));

        } else if (video_type.equals("dailymotion")) {

            player.setVisibility(View.GONE);
            playerWebView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            playerWebView.load(video_id);

        } else if (video_type.equals("local")) {

            playerWebView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            player.setVisibility(View.VISIBLE);
            player.setCallback(this);
            player.setSource(Uri.parse(video_url));

        }

        playerWebView.setEventListener(new PlayerWebView.EventListener() {
            @Override
            public void onEvent(String event, HashMap<String, String> map) {
                switch (event) {
                    case "start":
                        progressBar.setVisibility(View.GONE);
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        playerWebView.pause();
        playerWebView.destroy();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        player.pause();
        super.onPause();
    }

    @Override
    public void onStarted(EasyVideoPlayer player) {

    }

    @Override
    public void onPaused(EasyVideoPlayer player) {
        player.pause();
    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {

    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }
}
