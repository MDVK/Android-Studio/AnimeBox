/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.APPMDVK;

import android.app.Application;
import android.content.Context;

import com.example.admin.animebox.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by mlord on 11/26/2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

public class BaseApp extends Application{
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }


    @Override
    public void onCreate() {
        super.onCreate();

        // Default Font App
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/school.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

//        Realm.init(this); //initialize other plugins


    }
}