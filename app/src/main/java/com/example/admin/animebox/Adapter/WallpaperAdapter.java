/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.Activity.WallpaperDetail;
import com.example.admin.animebox.JsonConfig.WallpaperList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;

import java.util.List;

/**
 * Created by admin on 30-06-2017.
 */

public class WallpaperAdapter extends RecyclerView.Adapter<WallpaperAdapter.ViewHolder> {

    private Activity activity;
    private List<WallpaperList> wallpaperLists;
    private int columnWidth;
    private Constant_Api constant_api;

    public WallpaperAdapter(Activity activity, List<WallpaperList> wallpaperLists) {
        this.activity = activity;
        this.wallpaperLists = wallpaperLists;
        constant_api = new Constant_Api(activity);
        Resources r = activity.getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, r.getDisplayMetrics());
        columnWidth = (int) ((constant_api.getScreenWidth() - ((3 + 1) * padding)) / 1);
    }

    @Override
    public WallpaperAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(activity).inflate(R.layout.wallpaper_adapter, parent, false);

        return new WallpaperAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final WallpaperAdapter.ViewHolder holder, final int position) {

        // holder.image_wallpaper.setScaleType(ImageView.ScaleType.CENTER_CROP);
        holder.image_wallpaper.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(columnWidth, columnWidth / 1);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        holder.view.setLayoutParams(params);

        Glide.with(activity)
                .load(wallpaperLists.get(position).getWallpaper_image())
                .centerCrop()
                .placeholder(R.drawable.ic_thumbnail)
                .into(holder.image_wallpaper);

        holder.textView_view_count.setText(wallpaperLists.get(position).getTotal_views());
        holder.textView_downlode_count.setText(wallpaperLists.get(position).getTotal_download());

        holder.image_wallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent wd_intent = new Intent(activity, WallpaperDetail.class); //ORI -> WallpaperDetail
                Constant_Api.wallpaperLists.clear();
                Constant_Api.wallpaperLists.addAll(wallpaperLists);
                wd_intent.putExtra("position", position);

                //CHECK
                activity.startActivity(wd_intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return wallpaperLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image_wallpaper;
        private TextView textView_downlode_count, textView_view_count;
        private View view;

        public ViewHolder(View itemView) {
            super(itemView);

            image_wallpaper = (ImageView) itemView.findViewById(R.id.image_wallpaper_adapter);
            view = itemView.findViewById(R.id.view_wallpaper_adapter);
            textView_view_count = (TextView) itemView.findViewById(R.id.textview_view_wallpaper_adapter);
            textView_downlode_count = (TextView) itemView.findViewById(R.id.textview_downlod_view_wallpaper_adapter);

        }
    }
}
