/*
 * --------------------------------------------
 * Hello World, Welcome to my code .
 * this SingleMovie i made for purpose only
 * code create by admin on 29-06-2017.
 * --------------------------------------------
 * have a question, contact me :
 * 0852 1708 7944
 */

package com.example.admin.animebox.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.admin.animebox.JsonConfig.MovieInfoList;
import com.example.admin.animebox.R;
import com.example.admin.animebox.Util.Constant_Api;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by admin on 21-07-2017.
 */

public class MovieInfoFragment extends Fragment {

    private AVLoadingIndicatorView progressBar;
    private AdView mAdView;
    private ImageView imageView_movie_info, imageView_play;
    private TextView textView_title, textView_sub_title, textView_date, textView_description;
    private MovieInfoList movieInfoList;
    private Constant_Api constant_api;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.movie_info_fragment, container, false);

        constant_api = new Constant_Api(getActivity());

        progressBar = (AVLoadingIndicatorView) view.findViewById(R.id.progresbar_movie_info_fragment);
        imageView_movie_info = (ImageView) view.findViewById(R.id.imageView_movie_info_fragment);
        imageView_play = (ImageView) view.findViewById(R.id.imageView_play_movie_info_fragment);
        textView_title = (TextView) view.findViewById(R.id.textView_title_movie_info_fragment);
        textView_sub_title = (TextView) view.findViewById(R.id.textView_Sub_title_movie_info_fragment);
        textView_date = (TextView) view.findViewById(R.id.textView_date_movie_info_fragment);
        textView_description = (TextView) view.findViewById(R.id.textView_description_movie_info_fragment);

        mAdView = (AdView) view.findViewById(R.id.adView_movie_info_fragment);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (Constant_Api.isNetworkAvailable(getActivity())) {
            loadMovieInfo();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
            progressBar.hide();
        }

        imageView_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Constant_Api.isAppInstalled(getActivity())) {
                    Intent intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(), Constant_Api.YOUR_DEVELOPER_KEY, movieInfoList.getVideo_id(), 0, true, false);
                    getActivity().startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), R.string.youtube, Toast.LENGTH_SHORT).show();
                }
            }
        });

        setHasOptionsMenu(true);
        return view;
    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.movieinfofragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_share_movieInfo:
                Constant_Api.share = true;
                constant_api.ClassCall(movieInfoList.getVideo_image(), "movieInfo",movieInfoList.getVideo_url());
                Toast.makeText(getActivity(), "Share", Toast.LENGTH_SHORT).show();
                return true;
            // action with ID action_refresh was selected
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadMovieInfo() {

        progressBar.isShown();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant_Api.movie_info, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                Log.d("Response", new String(responseBody));
                String res = new String(responseBody);

                try {
                    JSONObject jsonObject = new JSONObject(res);

                    JSONArray jsonArray = jsonObject.getJSONArray(Constant_Api.MDVK);

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        String title = object.getString("title");
                        String sub_title = object.getString("sub_title");
                        String video_url = object.getString("video_url");
                        String video_id = object.getString("video_id");
                        String video_image = object.getString("video_image");
                        String description = object.getString("description");
                        String home_date = object.getString("home_date");

                        movieInfoList = new MovieInfoList(title, sub_title, video_url, video_id, video_image, description, home_date);

                    }

                    progressBar.hide();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Glide.with(getActivity()).load(movieInfoList.getVideo_image()).into(imageView_movie_info);
                textView_title.setText(movieInfoList.getTitle());
                textView_sub_title.setText(movieInfoList.getSub_title());
                textView_date.setText(movieInfoList.getDate());
                textView_description.setText(Html.fromHtml(movieInfoList.getDescription()));

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

}
