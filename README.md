# **ANiME BoX**
---

### #HowtoInstall 
#### do not Download, Please just **CLONE** this project . 
.
#### #android-gradle-template
a template for android with gradle

#### #Author
Aditya **Pratama**

#### #Features

Wallpaper with `glide, UIL, picasso`
Slide Show viewadapter
Music `EXo Media Player`
Database Handler `SQL Lite`
Youtube API
One Signal 
Firebase
Vimeo API
and much other feature ..



#### #Requirements

* Gradle 2.10, 2.11 or 2.12 with the plugin 2.10.0
* SDK with Build Tools 23.1+. Some features may require a more recent version.

##### **HOW TO Learn how to develop with IntelliJ/Android Studio and Gradle.**
1. http://snowdream.github.io/blog/android/2013/12/25/how-to-develop-with-android-gradle-template/
2. Gradle Plugin User Guide http://tools.android.com/tech-docs/new-build-system/user-guide
3. Gradle User Guide http://www.gradle.org/docs/current/userguide/userguide.html
---

## Screen Shoot
---

![Alt description](http://apaini-as.cloud.revoluz.io/public/upload/1.jpg)
![Alt description](http://apaini-as.cloud.revoluz.io/public/upload/3.jpg)
![Alt description](http://apaini-as.cloud.revoluz.io/public/upload/5.jpg)
---

## License
---

```
Copyright (C) 2017 MDVK <abehbatre@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```




